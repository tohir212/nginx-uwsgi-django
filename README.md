# nginx-uwsgi-django

## Daftar direktori nginx:

```
[root@server-x nginx]# pwd
/etc/nginx
[root@server-x nginx]# tree .
.
├── conf.d
│   ├── api.xyz.id.conf -> /etc/nginx/sites/api/api.xyz.id.conf
├── fastcgi_params
├── koi-utf
├── koi-win
├── mime.types
├── modules -> ../../usr/lib64/nginx/modules
├── nginx.conf
├── pma_pass
├── scgi_params
├── sites
│   ├── api
│   │   ├── api.xyz.id.conf
├── uwsgi_params
└── win-utf
```

## Daftar direktori uwsgi:
```
[root@server-x sites]# tree /etc/uwsgi/sites/
/etc/uwsgi/sites/
├── api.xyz.id-uwsgi.ini
├── peduli.api.xyz.id-uwsgi.ini
└── xyz.xyz.id.ini

0 directories, 3 files
```

## Daftar direktori api django

```
[root@server-x sites]# tree -L 1 /usr/share/nginx/html/api/api.xyz.id-uwsgi/
/usr/share/nginx/html/api/api.xyz.id-uwsgi/
├── api
├── apps
├── assets
├── env
├── gateway
├── log
├── manage.py
├── README.md
├── requirements.txt
├── static
├── templates
└── templatetags
```
9 directories, 3 files



